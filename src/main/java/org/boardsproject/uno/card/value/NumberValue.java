package org.boardsproject.uno.card.value;

import lombok.Value;

@Value(staticConstructor = "of")
public class NumberValue implements CardValue {

    int number;

}
