package org.boardsproject.uno.card.value;

public enum CustomValue implements CardValue {
    ZERO,
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    PASS,
    REVERSE,
    TAKE_TWO,
    TAKE_FOUR,
    CHOSE_COLOR
}
