package org.boardsproject.uno.card;

import lombok.Value;
import org.boardsproject.uno.card.value.CardValue;
import org.boardsproject.uno.card.value.CustomValue;
import org.boardsproject.uno.card.value.NumberValue;

@Value
public class Card {

    Color color;

    CardValue cardValue;

    public static Card common(int value, Color color) {
        return new Card(color, NumberValue.of(value));
    }

    public static Card pass(Color color) {
        return new Card(color, CustomValue.PASS);
    }

    public static Card reverse(Color color) {
        return new Card(color, CustomValue.REVERSE);
    }

    public static Card takeTwo(Color color) {
        return new Card(color, CustomValue.TAKE_TWO);
    }

    public static Card choseColor() {
        return new Card(Color.BLACK, CustomValue.CHOSE_COLOR);
    }

    public static Card takeFour() {
        return new Card(Color.BLACK, CustomValue.TAKE_FOUR);
    }

}
