package org.boardsproject.uno;

enum Direction {
    CLOCKWISE,
    ANTI_CLOCKWISE
}
