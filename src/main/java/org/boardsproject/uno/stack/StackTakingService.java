package org.boardsproject.uno.stack;

interface StackTakingService {

    StackTaking take(Stack stack, int amount);

}
