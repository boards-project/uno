package org.boardsproject.uno.stack;

import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import org.boardsproject.uno.card.Card;
import java.util.List;

@Value(staticConstructor = "of")
@Builder
public class Stack {

    @Singular
    List<Card> cards;
}
