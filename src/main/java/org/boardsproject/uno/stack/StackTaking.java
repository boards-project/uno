package org.boardsproject.uno.stack;

import lombok.Value;

@Value(staticConstructor = "of")
public class StackTaking {

    Stack taken;

    Stack remain;

}
