package org.boardsproject.uno.stack;

import lombok.NonNull;
import lombok.Value;
import org.boardsproject.uno.card.Card;

import java.util.List;

@Value
public class StackTakingServiceDefault implements StackTakingService {

    @Override
    public StackTaking take(@NonNull Stack stack, int amount) {
        if(amount > stack.cards().size()) {
            throw new IllegalArgumentException("amount is great than cards in stack");
        }

        final List<Card> cards = stack.cards();

        return StackTaking.of(
                new Stack(cards.subList(0, amount)),
                new Stack(cards.subList(amount, cards.size()))
        );
    }

}
