package org.boardsproject.uno.stack;

import lombok.Value;
import org.boardsproject.uno.card.Card;
import org.boardsproject.uno.card.Color;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Value
class StackFactory {

    @NotNull
    public Stack all() {
        List<Card> cards = new ArrayList<>();

        final List<Color> colors = Arrays.asList(
                Color.RED,
                Color.YELLOW,
                Color.GREEN,
                Color.BLUE
        );

        for (Color color : colors) {
            cards.add(Card.common(0, color));

            for (int value = 1; value < 10; value++) {
                cards.addAll(Collections.nCopies(2, Card.common(value, color)));
            }

            cards.addAll(Collections.nCopies(2, Card.pass(color)));
            cards.addAll(Collections.nCopies(2, Card.reverse(color)));
            cards.addAll(Collections.nCopies(2, Card.takeTwo(color)));

            cards.add(Card.choseColor());
            cards.add(Card.takeFour());
        }

        return new Stack(cards);
    }

}
