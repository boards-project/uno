package org.boardsproject.uno;

import lombok.Value;
import org.boardsproject.uno.stack.Stack;

@Value(staticConstructor = "of")
class Player {

    Stack stack;

}
