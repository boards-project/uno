package org.boardsproject.uno;

import lombok.Value;

import java.util.List;

@Value
class Game {

    List<Player> players;

    Direction direction;

}
