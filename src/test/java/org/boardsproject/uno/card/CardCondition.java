package org.boardsproject.uno.card;

import org.assertj.core.api.Condition;
import org.boardsproject.uno.card.value.CardValue;

import java.util.Objects;

public class CardCondition {

    public static Condition<Card> withColor(Color color) {
        return new Condition<>(
                card -> card.color() == color,
                "with color %s", color.colorName()
        );
    }

    public static Condition<Card> withValue(CardValue cardValue) {
        return new Condition<>(
                card -> Objects.equals(card.cardValue(), cardValue),
                "with value %s", cardValue
        );
    }
}
