package org.boardsproject.uno.stack;

import org.boardsproject.uno.card.Color;
import org.boardsproject.uno.card.value.CustomValue;
import org.boardsproject.uno.card.value.NumberValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.boardsproject.uno.card.CardCondition.*;

class StackFactoryTest {

    private StackFactory stackFactory;

    @BeforeEach
    void setUp() {
        stackFactory = new StackFactory();
    }

    @Nested
    class All {

        private Stack stack;

        @BeforeEach
        void setUp() {
            stack = stackFactory.all();
        }

        @Test
        void totalPackSize() {
            assertThat(stack.cards())
                    .hasSize(108);
        }

        @Test
        void content() {
            assertThat(stack.cards())
                    .haveExactly(25, withColor(Color.RED))
                    .haveExactly(25, withColor(Color.YELLOW))
                    .haveExactly(25, withColor(Color.GREEN))
                    .haveExactly(25, withColor(Color.BLUE))
                    .haveExactly(4, withValue(NumberValue.of(0)))
                    .haveExactly(8, withValue(NumberValue.of(1)))
                    .haveExactly(8, withValue(NumberValue.of(2)))
                    .haveExactly(8, withValue(NumberValue.of(3)))
                    .haveExactly(8, withValue(NumberValue.of(4)))
                    .haveExactly(8, withValue(NumberValue.of(5)))
                    .haveExactly(8, withValue(NumberValue.of(6)))
                    .haveExactly(8, withValue(NumberValue.of(7)))
                    .haveExactly(8, withValue(NumberValue.of(8)))
                    .haveExactly(8, withValue(NumberValue.of(9)))
                    .haveExactly(8, withValue(CustomValue.PASS))
                    .haveExactly(8, withValue(CustomValue.REVERSE))
                    .haveExactly(8, withValue(CustomValue.TAKE_TWO))
                    .haveExactly(4, withValue(CustomValue.CHOSE_COLOR))
                    .haveExactly(4, withValue(CustomValue.TAKE_FOUR));
        }
    }

}