package org.boardsproject.uno.stack;

import org.boardsproject.uno.card.Card;
import org.boardsproject.uno.card.Color;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

class StackTakingServiceDefaultTest {

    private StackTakingService stackTakingService;

    @BeforeEach
    void setUp() {
        stackTakingService = new StackTakingServiceDefault();
    }

    private static Stream<Arguments> source() {
        return Stream.of(
                Arguments.of(
                        Stack.builder()
                                .card(Card.pass(Color.YELLOW))
                                .card(Card.common(7, Color.RED))
                                .card(Card.common(7, Color.RED))
                                .build(),
                        2,
                        StackTaking.of(
                                Stack.builder()
                                        .card(Card.pass(Color.YELLOW))
                                        .card(Card.common(7, Color.RED))
                                        .build(),
                                Stack.builder()
                                        .card(Card.common(7, Color.RED))
                                        .build()
                        )
                )
        );
    }

    @MethodSource("source")
    @ParameterizedTest
    void take(Stack stack, int amount, StackTaking expected) {
        assertThat(stackTakingService.take(stack, amount))
                .isEqualTo(expected);
    }

    @Test
    void takeMoreThanThereIs() {
        final Stack stack = Stack.builder()
                .card(Card.common(7, Color.RED))
                .build();

        assertThatThrownBy(() -> stackTakingService.take(stack, 2))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("amount is great than cards in stack")
                .hasNoCause();
    }

    @Test
    void nullStack() {
        assertThatThrownBy(() -> stackTakingService.take(null, 2))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("stack is null")
                .hasNoCause();
    }
}